import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/components/Home/Home'
import OpenCase from '@/components/OpenCase/OpenCase'
// import Cases from '@/components/Cases/Cases'
// import News from '@/components/News/News'
// import Auth from '@/components/Auth/Auth'
// import Inventory from '@/components/Inventory/Inventory'
// import Roulette from '@/components/Roulette/Roulette'
// import Contracts from '@/components/Contracts/Contracts'
// import Contract from '@/components/Contracts/Contract'
// import Coinflip from '@/components/Coinflip/Coinflip'
// import CasesInfo from '@/components/Cases/Info'
// import RouletteInfo from '@/components/Roulette/Info'
// import ContractsInfo from '@/components/Contracts/Info'
// import CoinflipInfo from '@/components/Coinflip/Info'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'OpenCase',
      component: OpenCase,
      meta: {
        requiresAuth: true,
        title: 'Opening case'
      }
    }
  ]
})
